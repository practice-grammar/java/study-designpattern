package separate.bridge;

public abstract class SortAlgorithmImpl {
    public abstract void rawSort();
    public abstract void rawPrint();
}
